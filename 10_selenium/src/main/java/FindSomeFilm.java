import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.time.Duration;

public class FindSomeFilm {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "day08_selenium/src/main/driver/chromedriver/chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // TODO code here
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        driver.get("https://www.kinopoisk.ru/s/");

        Select select = new Select(driver.findElement(By.cssSelector("#country")));

//        select.getOptions().forEach(webElement-> {
//            System.out.println(webElement.getText());
//        });

        select.selectByVisibleText("Япония");

        Select multi = new Select(driver.findElement(By.xpath("//select[contains(@id, 'genre')]")));
        multi.getOptions().forEach(webElement-> System.out.println(webElement.getText()));

        System.out.println(multi.isMultiple());

        multi.selectByVisibleText("ужасы");
        multi.selectByVisibleText("фантастика");

        try {
            Thread.sleep(15_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement button = driver.findElement(By.xpath("//form[@name = 'film_search']/input[@value = 'поиск']"));
        button.click();

        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }
}