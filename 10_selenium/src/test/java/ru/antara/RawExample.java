package ru.antara;

import java.time.Duration;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RawExample {
    private static WebDriver driver;

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of("унесенные", "Япония", "Унесённые призраками", 5, "Унесенные волками (сериал)", new String[]{"аниме", "драма"})
        );
    }

    @BeforeAll
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "C:\\projects\\school\\antara_java_at_2021_09\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @AfterAll
    public static void teardown() {
        driver.quit();
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void searchResultTest(String searchStr, String country, String topResult, int size, String secondResult, String ...genres){

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        driver.get("https://www.kinopoisk.ru/s/");

        WebElement searchForm = driver.findElement(By.cssSelector("#formSearchMain"));

        WebElement textArea = searchForm.findElement(By.cssSelector("input[name='m_act[find]']"));
        textArea.sendKeys(searchStr);

        Select countrySelect = new Select(searchForm.findElement(By.cssSelector("#country")));
        countrySelect.selectByVisibleText(country);

        Select genreSelect = new Select(searchForm.findElement(By.cssSelector("select[name='m_act[genre][]']")));
        for (String genre: genres) {
            genreSelect.selectByVisibleText(genre);
        }

        WebElement searchButton = searchForm.findElement(By.cssSelector("input[value='поиск']"));
        searchButton.click();

        List<WebElement> results = driver.findElements(By.xpath("//div[contains(@class,'search_results')]/div[contains(@class,'element')]//p[@class='name']/a"));
        assertEquals(results.get(0).getText(), topResult);
        assertTrue(results.size() > size);
        assertTrue(results.stream().map(WebElement::getText).anyMatch(text -> text.equals(secondResult)));

        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }

        driver.quit();
    }
}
